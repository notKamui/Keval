rootProject.name = "Keval"
include("keval")

pluginManagement {
    repositories {
        gradlePluginPortal()
        mavenCentral()
    }
}
